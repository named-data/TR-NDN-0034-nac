\section{Evaluation}
\label{sec:analysis}

We perform a comparative evaluation on NAC.
We first compare NAC with CCN-AC~\cite{kurihara2015encryption}, another encryption-based access control scheme using regular public key cryptography, about the overhead of crypto computation and key retrieval.
We examine the difference between NAC and Attribute-Based Encryption~\cite{abe-ac}, another encryption scheme which is often mentioned as an easy-to-use solution for encryption-based access control. 


\subsection{NAC vs. CCN Access Control}

CCN-AC~\cite{kurihara2015encryption} is an encryption-based access control scheme where each data producer has complete knowledge about the access control policy, i.e., who are the authorized consumers and what the consumers are allowed to access.
We first consider the total number of encryption/decryption operations that both NAC and CCN-AC must perform to distribute content keys.
We compare the two schemes under two scenarios. 

The first scenario includes $m$ producers under the same namespace and $n$ consumers that are authorized to access data under the namespace for one day.
For both schemes, we assume that each producer will generate per-hour content key to encrypt data it produces within each hour. 

In CCN-AC, each producer needs to explicitly encrypt the content key for each authorized consumer. 
Therefore, the total number of encryption operations in one day can be calculated as: 
\begin{equation}
N_{ccn-ac} = 24mn
\end{equation}
For NAC, a data owner can create a consumption credential for all the data produced in one day and encrypt the credential KDK for each authorized consumer.
Each producer only needs to encrypt each content key with the credential KEK.
\begin{equation}
N_{nac} = 24m + n
\end{equation}
Clearly, the number of encryption operations that NAC has to perform is much less than CCN-AC, especially in cases of a large number of producers or consumers.

In the second scenario, we also consider $m$ producers but 24 consumer groups, and each group has $n$ consumers.
Each group of consumers can access data produced in a particular hour of a given day. 
In CCN-AC, each producer will encrypt a content key for all the authorized consumers:  
\begin{equation}
N_{ccn-ac} = 24m + 24mn
\end{equation}
For NAC, a data owner needs to create 24 credentials.
A data owner will encrypt each KDK for authorized consumers, while each data producer encrypts the content key using the corresponding credential KEKs:
\begin{equation}
N_{nac} = 24m + 24n
\end{equation}
The result suggests that NAC scales better than CCN-AC when there is more than one producers in the system.
Note that the scalability comes from the one-level indirection of data owner, which aggregates multiple consumers into a group, so that data producers only need to be aware of the group's KEK rather than the key of each group member. 

Our second evaluation metric is the overhead in key retrieval.
CCN-AC puts all encrypted content keys into a single data blob, called {\em manifest}.
A consumer must retrieve the whole manifest to extract the content key encrypted for it.
When the size of the data blob is larger than the network's maximum transmission unit (MTU), the data blob must be segmented.
Assume that MTU is 1500 bytes and that consumers use RSA keys, one data segment can carry about 4 encrypted content keys.
Assuming that a content key is granted to $n$ consumers, the average number of data packets that a consumer must retrieve is:
\begin{equation}
N_p = \lceil\lceil n/4\rceil \cdot 2 \rceil
\end{equation}
This analysis suggests that a CCN-AC consumer only needs to retrieve one key packet when there are fewer than 4 authorized consumers for a content key, but will need to retrieve multiple key packets when the number of authorized consumers increases, and the number increases linearly with the number of authorized consumers. 
In contrast, NAC requires a consumer to retrieve a content key and a key-decrypt key, of which each is carried by a separate data packet, therefore only two data packets are needed to retrieve these two keys.

\subsection{NAC vs. Attribute-Based Encryption}
%Attribute-Based Encryption (ABE)~\cite{abe-ac}, another encryption scheme, is frequently mentioned as an alternative to encryption-based access control.
%In the rest of this section, we compare the ABE based access control and NAC.

Attribute-based encryption (ABE) was firstly proposed by Sahai and Waters~\cite{abe}.
In ABE, user's credential is composed by a set of attributes; each attribute is a descriptive, readable string indeed.
A policy (e.g.,  an "AND" and "OR" formula) over these attributes serves as the predicate.
In particular, there are two main forms of ABE: Key-Policy ABE~\cite{abe-ac} where the policy is ascribed to user's decryption key and Ciphtertext-Policy ABE~\cite{cp-abe} which binds the policy to ciphertext.

Before we make a brief comparison between NAC and Attribute-Based Encryption 
, it is helpful to understand the working mechanism of Key-Policy ABE (Figure~\ref{fig:abe-nac}(b)). 
Key-Policy ABE encrypts data using a set of predefined, descriptive attributes, eliminating the need for data producers to fetch encryption keys. 
To enable such a scheme, ABE requires a key authority that knows the attributes of all the receivers and can generate a {\em master key} and its corresponding {\em public params}.
An ABE receiver (data consumer) must obtain its private key from this key authority.
The key authority derives a user's private key from the master key together with the user's policy over attributes.
Users with the identical attribute policy will obtain the same private key.
An ABE sender (data producer) generates ciphertext using the public params together with a set of attributes.
A receiver can decrypt a ciphertext only if the receiver's attribute policy matches the attributes with which the ciphertext is generated.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.55]{abe-nac}
  \caption{Comparison between attribute-based encryption and name-based access control}
  \label{fig:abe-nac}
\end{figure}

Figure~\ref{fig:abe-nac} shows the working flow of both NAC and Key-Policy ABE. 
Next, we will compare Key-Policy ABE with NAC on five aspects: fine-grained access control, setup, encryption, decryption, and revocation.

\subsubsection{Fine-grained access control}

Both NAC and Key-Policy ABE are fine-grained access control scheme but achieve the goal in different way.
As mentioned in previous section, NAC achieves fine-grained access control by naming convention.
A particularly designed name let data owner have ability to specify the privilege and restrict credential set for each consumer.
For example, embedding timestamp to packet name would achieve time granularity of access control.

Key-Policy ABE achieves fine-grained access control by using the attribute policy.
The policy can combine different attributes flexibly and semantically.
Users with different attribute policy could access to different subset of encrypted content.

\subsubsection{Setup}

Both NAC and Key-Policy ABE requires an authority (the data owner or key authority) that determine how encryption and decryption keys match up.
In NAC, the encryption and decryption keys are generated by the data owner.
The data owner names the encryption/decryption keys by their encryption scope and signs the keys.
Both producers and consumers must learn the {\it owner public key} at the setup phase, so that they can authenticate the encryption/decryption keys and use them properly.

In Key-Policy ABE, an encryption key can be constructed by any sender, and the corresponding decryption key is derived by the key authority.
In order to pair up encryption and decryption keys, a sender must learn the {\it public parameters} of the key authority at the setup phase, and use them to generate encryption key correctly.
Receivers must learn certain information (e.g., key authority's public key) so that they can securely request decryption keys from the key authority.

The Key-Policy ABE scheme, however, also requires each sender to be configured with the knowledge about all the supported attributes, i.e., which attributes should be used to encrypt which data.
In contrast, NAC does not require such configuration, but relies on the encryption naming convention to determine which keys should be used to encrypt a particular piece of data.

\subsubsection{Encryption}

NAC and Key-Policy ABE differ most significantly in encryption. 
NAC requires producers to periodically retrieve KEKs from data owners, thus introducing the overhead of key retrieval.
While failure in retrieving a KEK does not block data production, it may prevent consumers from accessing the data.
In contrast, Key-Policy ABE waives the need of key retrieval.
A sender can directly encrypt data with a set of descriptive attributes. 
In order to control the time granularity of encryption, the authority can use the time as one of the attributes. 

The computational overhead of two schemes are determined by different factors.
In NAC, a producer encrypts data using a content key, which in turn is encrypted by one or more key-encrypt keys.
In the worst cases where there is a key-encrypt key at each level of the key hierarchy, the computational overhead is proportional to the depth of the key hierarchy. 
However, in Key-Policy ABE, a producer may need to process each attribute used in encryption, the computational overhead will linearly increase with the number of attributes used in the encryption. 

\subsubsection{Decryption}

Both NAC and Key-Policy ABE relie on the authority (data owner or key authority) to generate decryption keys and distribute the keys to corresponding consumers if they have authenticated themselves to the authority.
In both scheme, the generated decryption keys must be securely delivered to the consumers.

NAC creates a decryption key hierarchy and assign consumers into the different levels in the hierarchy according to the consumer's privileges.
Key-Policy ABE expresses a consumer's privilege in terms of the consumer's attribute policy and crafts decryption keys according to the policy.
While Key-Policy ABE waives the need of maintaining the decryption key hierarchy, the process of key generating and decryption is non-trivial, and may introduce significant computation overhead. 

\subsubsection{Revocation}

Both NAC and Key-Policy ABE handle revocation through restricting the livetime of encryption keys.
In both schemes, consumers have to periodically ``renew'' the corresponding decryption keys.
In NAC, data owner explicitly specifies the lifetime of KEKs in the key name.
As a result, the data owner controls the temporal granularity of keys.
In contrast, senders in Key-Policy ABE specify the validity period of the encryption key as an attribute involved in the encryption.
When the validity period of a receiver's decryption key does not satisfy the required attributes, the receiver must request a new decryption key with appropriate validity period from the key authority. 
Therefore, the lifetime of encryption keys is usually determined by the senders in Key-Policy ABE.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "gep"
%%% End:
