\section{Named Access Control}
\label{sec:nac}

In this section, we first explain how to name data, followed by the explanation on how to name production credential (signing/verification keys) and consumption credential (key-decrypt/decrypt keys) to specify different access privileges.
We will also show how to distribute keys in a distributed system to achieve the content-based access control and discuss how to revoke the access.

\subsection{Naming Data}
In NDN, data is named under a hierarchical namespace.
This allows us to group data with the same property into the same namespace.
As an illustrative example, Figure~\ref{fig:naming-tree} shows the naming hierarchy for Alice's health data.
Alice can put all her health related data (including keys as we will show later) under a namespace \ndnName{/alice/health}.
Under this namespace, Alice allocates a sub-namespace \ndnName{/alice/health/samples} for the data produced by sensors.
Alice can further sort her health data into two categories: \ndnName{activity} and \ndnName{medical}, and give each of them an individual namespace: \ndnName{/alice/health/samples\allowbreak/activity} and \ndnName{/alice\allowbreak/health\allowbreak/samples/medical}.
The activity namespace covers two types of data: steps (\ndnName{alice/health/samples\allowbreak/activity\allowbreak/steps}) and location (\ndnName{/alice/health/samples\allowbreak/activity\allowbreak/location}).
Each piece of data is named under the namespace for its own type, with a suffix that can describe additional information of the data.
For example, the data under name \ndnName{/alice/health/samples\allowbreak/activity\allowbreak/steps\allowbreak/2015\allowbreak/08\allowbreak/27/16/30} refers to the step data that is produced during 16:30 to 16:31 on August 27th, 2015 (assuming activities are measured in the time unit of one minute).

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{naming-tree}
  \caption{An example of naming hierarchy for Alice's health data}
  \label{fig:naming-tree}
\end{figure}

\subsection{Naming Production Credential}
Since data is organized under the hierarchical naming structure, a data owner can express the privilege of a production credential as the namespace under which the producer is authorized to produce data.
For example, a namespace \ndnName{/alice/health/samples/activity} represents the privilege of producing Alice's activity data, including both step and location.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{signing-key-name}
  \caption{The naming convention of signing key}
  \label{fig:signing-key-name}
\end{figure}

To authorize a data producer to produce data under a given data namespace, a data owner can issue a signing key certificate which associates the producer's signing key with the authorized namespace.
Figure~\ref{fig:signing-key-name} shows the naming convention of signing key.
A signing key name consists of three parts:
1) a prefix indicating the data namespace;
2) a key ID that with the prefix uniquely identifies the signing key;
and 3) a special name component \ndnName{KEY} that distinguishes the key from normal data under the same name.

We mentioned in Section~\ref{sec:ndn} that NDN requires data producer to put its signing key name into the \ndnName{KeyLocator} field of each produced data packet. 
With this information, a data consumer can construct a chain of signing keys from the producer signing key to a trusted key it already knew (e.g., Alice's root key).
Consumers can check the authentication chain against a pre-defined trust model, which can be described in a trust schema~\cite{schema}, to decide whether a producer has been authorized to produce a particular data packet.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{signing-key-hierarchy}
  \caption{Signing hierarchy of Alice's health data}
  \label{fig:signing-key-hierarchy}
\end{figure}

Figure~\ref{fig:signing-key-hierarchy} shows an example trust model for Alice's production credential authentication.
This hierarchical trust model has the root key of Alice's own namespace \ndnName{/alice} as the trust anchor.
A separate key, \ndnName{/alice/health/5fdf51/KEY}, is created to manage the wellness sub-namespace (\ndnName{/alice/health}), which is used to sign the certificate of each authorized data producer (pulse sensor and activity sensor).

A data owner may adopt different trust model for the production credential authentication.  Discussions on alternative trust models are beyond the scope of this paper.  Interested readers are referred to our previous work~\cite{schema} about trust management in NDN.

\subsection{Naming Consumption Credential}
In our design consumption credential is another public key pair (KEK/KDK) for content key encryption.
We use well defined naming convention to help a data owner explicitly specify the privilege of a consumption credential.
%% Each consumer credential gives the access right to a data set, that can be decrypted by a consumer with the decryption key.
We mentioned earlier that the privilege of a consumption credential is a data set that a consumer with the key-decrypt key (KDK) can access (indirectly through decrypted content keys).
With the privilege encoded in the corresponding key-encrypt key (KEK) name, a data producer can tell which content key should or should not be encrypted through the key-encrypt key.

The naming of key-encrypt/decrypt key (KEK/KDK) must accommodate four facts.
First, key-encrypt/decrypt keys have different usage than the signing/verification keys introduced above. 
A signing key is possessed by an authorized producer while a KDK is held by an authorized consumer.
The roles of these two pairs of keys are different, and the key name must explicitly reflect such differences.

Second, consumption credential is created and managed by data owner. 
The naming convention of consumption credential should prevent other entities (e.g.,  producers or consumers) from issuing valid consumption credential.

Third, a data owner may want to delegate the consumption credential management to a third party.  
The consumption credential naming convention should facilitate such management delegation, at the same time, a data owner must be able to restrict the privilege of this third party to consumption credential management only.
In other word, the third-party entity should not be able to produce wellness data on behalf of the data owner.

Last, the data set that a consumption credential covers may need additional description that cannot be explicitly encoded as the data namespace.
For example, a data owner may want to create a consumption credential that allows consumers to access data produced during certain time period, e.g., from 6pm to 10pm on every workday.
Therefore, the consumption credential name must be able to carry additional information to enforce a variety of access restrictions beyond the data naming hierarchy.

We will present a naming design that can address the four issues above.

\subsubsection{Consumption credential namespace}
To distinguish consumption credentials apart from production credentials, we allocate a separate namespace for consumption credential, which is parallel to the data namespace as shown in Figure~\ref{fig:read-access-name-tree}.
Take Alice's health data as an example, Alice can create a namespace \ndnName{/alice/health\allowbreak/read} for consumption credentials.
The naming hierarchy of the consumption credential namespace mirrors that of the data namespace, except that data under this hierarchy are all consumption credentials.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{read-access-name-tree}
  \caption{An example of consumption credential namespace along with data namespace}
  \label{fig:read-access-name-tree}
\end{figure}

With a separate consumption credential namespace, a data owner can delegate the whole or part of the consumption credential management to a third party.
The data owner can publish the delegation as a certificate which binds the third party's public key to the delegated consumption credential namespace or sub-namespace.
Figure~\ref{fig:credential-mgmt-key-name} shows an example of consumption credential delegation in which Alice delegated her physician to control the read access to her medical data.
As restricted by the certificate name, the delegated entity (e.g., Alice's physician) can only issue consumption credential for certain type of data (e.g., medical data), and cannot issue any production credential nor produce any data, including medical data.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{credential-mgmt-key-name}
  \caption{An example of consumption credential delegation.}
  \label{fig:credential-mgmt-key-name}
\end{figure}

The naming hierarchy under the consumption credential namespace also enables multi-level delegation. 
For example, Alice's physician can further delegate a cardiology expert to manage the read access to Alice's heart rate data.


\subsubsection{Consumption credential name convention}

Under the consumption credential namespace, a data owner can name a consumption credential at any level of the naming hierarchy (e.g., \ndnName{/alice/health/read/medical}, and \ndnName{/alice/health/read\allowbreak/medical/pulse}) with the meaning that consumers with the credential can only access data under the corresponding data namespace.
We mentioned earlier that our design uses a public key pair (KEK/KDK) to construct a consumption credential.
Both keys need to be named properly to convey the privilege of a consumption credential. 

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{e-key-name}
  \caption{The key naming convention of consumption credential}
  \label{fig:e-key-name}
\end{figure}

Figure~\ref{fig:e-key-name} shows the naming convention for the keys of a consumption credential.
Both key-encrypt key (KEK) and key-decrypt key (KDK) share the same naming structure. 
They all start with a particular prefix under the consumption credential naming hierarchy.
After the prefix, each type of keys has a key-tag component that distinguishes the usage of these keys: \ndnName{E-KEY} for key-encrypt key and \ndnName{D-KEY} for key-decrypt key.
After the key-tag, a data owner can append other additional restrictions that have not been explicitly encoded in the data namespace.
For example, the key names in Figure~\ref{fig:e-key-name} says that a consumer with this corresponding credential can access Alice's step data produced between 4pm to 6pm on August 27, 2015.

% For every (group) encryption key, a namespace manager needs to publish the corresponding decryption key encrypted using each authorized consumer's public key.
% If a namespace has more than one consumer, there will be multiple encrypted copies of the decryption key.
% Figure~\ref{fig:d-key-name} shows an the naming convention of encrypted decryption key.
% In order to distinguish (group) encryption key and decryption key, the decryption key name has a component \ndnName{D-KEY} (instead of \ndnName{E-KEY}) after the group namespace.
% In order to distinguish different encrypted copies, the corresponding consumer name is appended to the decryption key name.

% \begin{figure}[htbp]
%   \centering
%   \includegraphics[scale=0.4]{d-key-name}
%   \caption{The naming convention of group decryption key}
%   \label{fig:d-key-name}
% \end{figure}

% Note that this naming convention allows a producer to put group encryption key name directly into the \ndnName{DecryptionKeyName} field of a content key packet.
% A consumer can simply replace \ndnName{E-KEY} with \ndnName{D-KEY} and append its own name to obtain the name of its own copy of group decryption key,
% and use the constructed name to retrieve the decryption key.


% In content-based access control, one controls read access by controlling decryption key distribution.
% Before going into details about encryption-based read access control, let us consider a simple encryption scheme (Figure~\ref{fig:key-access}) which illustrates the problems that motivate the design later.

% \begin{figure}[htbp]
%   \centering
%   \includegraphics[scale=0.5]{key-access}
%   \caption{A simple encryption-based read access control}
%   \label{fig:key-access}
% \end{figure}

% In Figure~\ref{fig:key-access}, a data producer creates a content key (a symmetric key) and encrypts the produced data using the content key. 
% To allow a consumer to read the encrypted data, the producer encrypts the content key using the consumer's public key.
% The producer publishes both encrypted data and encrypted content key (Figure~\ref{fig:encrypted-data-format}). 
% After retrieving the encrypted data packet, a consumer can follow the {\tt DecryptionKeyName} to retrieve the packet that carries the encrypted content key.
% The {\tt DecryptionKeyName} in the content key packet points to the consumer's private key. 
% With the specified private key, a consumer can decrypt the content key and then decrypt the data.

% \begin{figure}[htbp]
%   \centering
%   \includegraphics[scale=0.5]{encrypted-data-format}
%   \caption{Data packets carrying encrypted data and keys}
%   \label{fig:encrypted-data-format}
% \end{figure}

% This simple scheme exposes two issues that we must address in design.
% First, a producer has to maintain an access control list which include the public key of each consumer and the data set they can read. 
% As we mentioned before, maintaining an ACL at the producer side leads to scalability concerns, therefore should be avoided.
% Second, when a data packet has multiple authorized consumers, its content key will also need to be encrypted multiple times, one for each of the authorized consumers, making it infeasible to carry the names of all these decryption key names in every data packet.
% Therefore, we need to define a naming convention for data packets that carry decryption key, to allow consumers easily retrieve their corresponding copy of the decryption key instead of carrying them in the packet.

\subsection{Credential Delivery}

For producer credential, we assume that a producer creates and retains signing key and data owner issues signing key certificate using conventional certificate issuing mechanism.
Only consumers need to retrieve signing key certificates for data authentication. 
Since a signing key certificate is an NDN data packet, data owner can simply upload the issued certificate to a data storage.
Potential data consumer can follow \ndnName{KeyLocator} in data packet to retrieve the certificate later.

The key-encrypt/decrypt key of a consumption credential, however, are created by data owner and should be delivered to related data producers and authorized consumers respectively.
We will explain how to deliver these keys to related entities.

\subsubsection{Key-encrypt key delivery}
As mentioned earlier, the key-encrypt key (KEK) in this design is a public key. 
A data owner can name a KEK as we mentioned above (Figure~\ref{fig:e-key-name}), and publish the key as a data packet.
Since each encryption key has the data owner's signature, they can be safely uploaded to the data storage, retrieved and verified by data producers and consumers.
As long as a data producer knows the key-encrypt key naming convention, it can infer the name of the key-encrypt key to retrieve.
The naming convention can be tailored for specific applications to facilitate key retrieval.

Let's consider the wellness application as an example. 
In this application, producers (e.g., sensors) produce wellness data continuously.
With the naming convention defined in Figure~\ref{fig:e-key-name}, a data owner can specialize the ``additional restriction'' as a time interval, and create a sequence of KEKs.
The time interval of these KEKs can be concatenated together to cover a continuous time period as shown in Figure~\ref{fig:time-interval}.
Note that this naming convention implies that the ending timestamp of a KEK is the starting timestamp of the next KEK.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{time-interval}
  \caption{A sequence of key-encrypt keys cover a continuous time period.}
  \label{fig:time-interval}
\end{figure}

To construct an interest to retrieve a KEK, a data producer must first determine the credential prefix.
Note that there could be multiple prefixes which a data producer can infer from the name of produced data.
For example, given Alice's step data \ndnName{/alice/health/samples\allowbreak/activity\allowbreak/step}, the activity sensor can derive the most specific credential prefix \ndnName{/alice/health\allowbreak/read\allowbreak/activity\allowbreak/step} corresponding to the step data namespace.
Since every parent credential prefix of the most specific prefix also covers the step data, the data producer can determine all the possible credential prefix by tracing back to the root of the credential namespace (e.g., \ndnName{/alice/health\allowbreak/read}).
In the example above, the activity sensor can deterministically derive three prefixes: \ndnName{/alice/health\allowbreak/read}, \ndnName{/alice/health\allowbreak/read/activity}, and \ndnName{/alice/health\allowbreak/read/activity/step}.

For each derived credential prefix, a data producer needs to determine the starting timestamp for the KEK to retrieve.
We mentioned earlier that the ending timestamp of a KEK is the starting timestamp of the next KEK.
When a data producer has already obtained a KEK, it can construct an interest for the next KEK by specifying the starting timestamp using the ending timestamp of the obtained key.
Routers and data storage can apply the longest prefix match to pick the next KEK and satisfy the interest.

If a data producer has not received any KEK before, the data producer can express an interest with the credential prefix (e.g., \ndnName{/alice/health\allowbreak/read/activity/E-KEY}).
The interest can bring back a KEK which can serve as a starting point for the KEK enumeration as described above.

When a retrieved KEK's ending timestamp is much earlier than current timestamp, KEK enumeration may become inefficient.
In this case, a data producer can use \ndnName{Selectors} to speed up the key enumeration process.
More specifically, a data producer may use \ndnName{Exclude} filter to exclude any KEK whose starting timestamp is earlier than the latest one among all the received KEKs.\footnote{In case clock is not synchronized, one may also set \ndnName{Exclude} filter to exclude any KEK whose starting timestamp is later than current timestamp.}
A data producer may also specify \ndnName{ChildSelector} to select the ``rightmost'' KEK under the credential prefix.

\subsubsection{Key-decrypt key delivery}

Key-decrypt key (KDK) should be visible only to authorized consumers.
Note that a data owner may not be online all the time, it would be desirable for the data owner to leave the KDKs in a data storage for authorized consumers to retrieve it whenever needed.
Since the data storage is untrusted, a data owner can encrypt a KDK using the public key of each authorized consumers.
Each encrypted copy makes an individual data packet.

Figure~\ref{fig:encrypted-data-format} shows the format of encrypted data.
The data content consists of two components: \ndnName{EncryptionAlgorithm} which the meta-information about the encryption scheme and \ndnName{EncryptedContent} which contains the cipher text of content.
Note that the format is general enough to carry any content which is not restricted to KDKs but also include content key and normal content.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{encrypted-data-format}
  \caption{Data packets carrying encrypted data and keys}
  \label{fig:encrypted-data-format}
\end{figure}

Since each consumer has its own encrypted copy of KDK, each copy must have a unique name.
To distinguish different copies, we define the naming convention for encrypted data as shown in Figure~\ref{fig:encrypt-data-name}.
For each encrypted copy, we append a special name component \ndnName{FOR} and the encrypting key name after the content name.
For example, a decryption key for Alice's activity data that is encrypted using Bob's public key is named as \ndnName{/alice/health/read\allowbreak/activity/D-KEY/20151016T000800/20151016T001800/FOR\allowbreak/bob\allowbreak/health/\allowbreak access/E-KEY}.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{encrypt-naming-convention}
  \caption{Naming convention of encrypted data}
  \label{fig:encrypt-data-name}
\end{figure}

%Unlike data producers, data consumers do not need to pro-actively retrieve the decryption key.
The name of encrypting key in each data packet can help a data consumer to construct a decryption chain to access the original content as shown in Figure~\ref{fig:decryption-chain}.
When a consumer retrieves an encrypted data packet, it can extract the content key name from the data name.
We assume that an authorized consumer should know its authorized credential prefix\footnote{An authorized consumer does not have to know the complete credential name, i.e., the full name of each decryption key.}. 
With the content key name, a consumer can construct an interest by appending the consumption credential prefix to the content key name.
With longest prefix match, routers and data storage can satisfy the interest with the encrypted content key.
After receiving encrypted content key, the consumer can extract the key-encrypt key (KEK) name and construct an interest for the corresponding key-decrypt key (KDK) by appending the consumer's own name to the KDK name.
In the end, the consumer can retrieve the encrypted KDK and recursively decrypt all the intermediate keys and the original content.
 
\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{decryption-chain}
  \caption{A chain of keys to decrypt wellness data}
  \label{fig:decryption-chain}
\end{figure}

\subsection{Fine-Grained Access Control}

With consumption credential, a data owner can control the read access to content from two dimensions: specifying the privilege of individual consumption credential and restricting the set of credentials that a consumer can obtain.
For example, Alice may want to share her location information with her husband Bob all the time, but with her colleague Cathy only during working hours. 
In this case, Alice can specify a sequence of consumption credentials which cover her location data for 9am-5pm and 5pm-9am every day.
Alice can encrypt the KDKs for 9am-5pm from Monday to Friday for both Bob and Cathy, and encrypt all the other decryption keys for Bob only, as shown in Figure~\ref{fig:fine-grained}.

\begin{figure}[htbp]
  \centering
  \includegraphics[scale=0.5]{fine-grained}
  \caption{Different read privilege in terms of KDK set.}
  \label{fig:fine-grained}
\end{figure}

In fact, a data owner can divide the data set arbitrarily into multiple consumption credentials (KEK/KDKs), so that the data owner can create different combination of KDKs to represent different privilege of individual consumers. 
When the read privilege can be pre-defined, consumption credentials can be automatically created and published in the network.

\subsubsection{Post-Facto Access Granting}

The model we discussed so far focus on controlling the access to data as they are being produced.
The name-based access control also allows a data owner to grant a new consumer the access to the data that is produced long time ago.
When the granted access is covered by one or more KDKs that were generated earlier, the data owner can simply encrypt KDKs directly using the new consumer's public key.

Note that a data owner can always create a top-level consumption credential (e.g., \ndnName{/alice/health/read}) and retain the KDKs to itself only.
Since every producer will publish a copy of content key encrypted using the KEK of the top-level credential, the data owner can obtain all the content keys.
As a result, when the granted access cannot be expressed as a combination of existing KDKs, the data owner can re-encrypt the granted content key directly for the new consumer.


\subsection{Access Revocation}

With content-based access control, revoking write access is equivalent to revoking the producer's public key certificate, so that neither data storage nor end consumers will accept data of the revoked producer.
A data owner can also easily prevent a previously authorized consumer from reading any new data by stopping publishing consumption credentials for the consumer.  
However, revoking data access that has been granted requires strict control on the availability of KDKs, i.e., preventing a revoked consumer from accessing these KDKs. 
For example, a data owner may delete from a data storage the KDKs encrypted for a revoked consumer. 

A more effective solution can be provided at the application layer. 
For example, to control the access to video with copy right, a video provider (e.g., Netflix, Hulu) may ship its own video player as a blackbox to user.
The video player not only decrypts the video stream, but also can prevents users from retaining a copy of the decrypted video.
We assume the same techniques can be applied here to control the access to the KDKs.
More specifically, the blackbox can negotiate an ephemeral keys for KDK distribution and throw the ephemeral keys and KDKs away after data decryption.
For a revoked consumer, the blackbox will fail to obtain an ephemeral key, thus preventing the consumer from accessing content.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "gep"
%%% End:
